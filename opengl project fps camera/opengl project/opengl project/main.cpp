#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <string>
#include <sstream>
#include <cmath>
#include "Shader.h"
#include "RainbowTriangle.h"
#include "Cube.h"
#include "Camera.h"

#include <glm/glm.hpp> //basic glm functions/types
#include <glm/gtc/matrix_transform.hpp> //glm matrix functions/types
#include <glm/gtc/type_ptr.hpp> //glm convert glm types to opengl types

#define STB_IMAGE_IMPLEMENTATION //so it wont go looking for stb_images c or cpp file to compile
#include "stb_image.h"

using namespace std;

bool wireFrame = false;


//camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));//make camera with starting xyz
float lastX = 400, lastY = 300; //half width and height to help with mouse movement checking
bool firstMouse = true; //help us make sure when mouse first read, dont jump camera angle to harshly

//Time Management stuff, too help with camera movement
float deltaTime = 0.0f; //time between last frame draw and now
float lastFrame = 0.0f; //time of last frame

//new callback functions for mouse input!
void mouse_callback(GLFWwindow* window, double xpos, double ypos);

//scroll wheel
void scroll_callback(GLFWwindow* window, double xOffset, double yOffset);



//window resize call back function, which will change glViewport to match new size
void windowResizeCallBack(GLFWwindow* window, int width, int height);

//user input function
void processInputs(GLFWwindow* window);

//show frames per second
void showFPS(GLFWwindow* window);

int currentShape = 3;


//Vertex Shader Program Source code
const char* vertexShadersource =
"#version 330 core\n"
"layout(location = 0) in vec3 aPos;\n" //e.g aPos representing positions can access like this aPos.x, aPos.y, aPos.z or even aPos[0] for the x, etc
"\n"
"void main()\n"
"{\n"
"gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"//main job of vertex shader is to pass a gl_position to help with drawing, gl_position takes a vec4. this position is used to help construct geometry
"}\n\0";//\0 tells a character string where to end

//Fragment Shader Program Source code
const char* fragmentShadersource =
"#version 330 core\n"
"out vec4 FragColor; \n"//value passed onto next shader when this one is done
"\n"
"void main()\n"
"{\n"
"FragColor = vec4(1.0f,0.5f, 0.2f, 1.0f);\n"
"}\n\0";


void main(){
	glfwInit();
	//tell glfw which version of openGL to use: 3.3 core profile
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);//the first 3 in 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);//the .3 of 3.3
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);//core profile

	//build a window! 800x600 window
	GLFWwindow* window = glfwCreateWindow(800, 600, "My first openGL thing", NULL, NULL);
	//did it fail?
	if (window == NULL){
		cout << "Window failed to build" << endl;
		glfwTerminate();
		system("pause");
		return;
	}
	//make this the main window
	glfwMakeContextCurrent(window);

	//setup GLAD to help initialise the openGL stuff
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		//if it fails
		cout << "GLAD failed to initialise" << endl;
		glfwTerminate();
		system("pause");
		return;
	}
	//tell opengl which part of our 800,600 window is used for opengl
	glViewport(0, 0, 800, 600);//x,y,w,h

	//turn on z buffering
	glEnable(GL_DEPTH_TEST);

	stbi_set_flip_vertically_on_load(true);

	//add window resize call back too the window, so when that event occurs, this function will be called for us
	glfwSetFramebufferSizeCallback(window, windowResizeCallBack);

	//register mouse call backs
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	//hide the cursor and lock mouse within window area
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	Shader shaderProgram1("vertexShader1.txt", "fragmentShader1.txt");
	Shader shaderProgram2("vertexShader2.txt", "fragmentShader2.txt");
	Shader shaderProgram4("projectionVertexShader1.txt", "textureFragmentShader2.txt");

	//SHADERS
	//generate a shader ID for our vertex shader
	int vertexShaderID = glCreateShader(GL_VERTEX_SHADER);//gives id of new vertex shader
	//give this vertex shader our source code
	//params: shader id to work with
	//		how many shader source code strings do you want to give it?
	//		the source code you want to give it
	//      length of source string, or NULL if source code ends in a \0
	glShaderSource(vertexShaderID, 1, &vertexShadersource, NULL);
	//compile the code
	glCompileShader(vertexShaderID);

	//check for errors
	int success; //failed or not
	char infoLog[512]; //store error information
	//check compile status
	glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &success);
	//if failed
	if (!success){
		//get error message and display it
		glGetShaderInfoLog(vertexShaderID, 512, NULL, infoLog);
		cout << "Vertex Shader error!" << infoLog << endl;
		system("pause");
	}
	
	//fragment shader!
	int fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderID, 1, &fragmentShadersource, NULL);
	glCompileShader(fragmentShaderID);

	glGetShaderiv(fragmentShaderID, GL_COMPILE_STATUS, &success);
	if (!success){
		glGetShaderInfoLog(fragmentShaderID, 512, NULL, infoLog);
		cout << "Fragment shader error!: " << infoLog << endl;
		system("pause");
	}
	//create a shader program which will link our whole graphics pipeline together
	int shaderProgramID = glCreateProgram();
	//attach our shaders
	glAttachShader(shaderProgramID, vertexShaderID);
	glAttachShader(shaderProgramID, fragmentShaderID);
	//link our shaders
	glLinkProgram(shaderProgramID);
	//check for shader linking errors
	glGetProgramiv(shaderProgramID, GL_LINK_STATUS, &success);
	if (!success){
		glGetProgramInfoLog(shaderProgramID, 512, NULL, infoLog);
		cout << "Shader Program Linking Error!: " << infoLog << endl;
		system("pause");
	}
	//once its linked together into 1 big shader program, we no longer need the individual smaller shaders
	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);

	//setup triangle vertices
	//a vertex is a 3d coordinate, colour, texture coordinates, normal directions, etc
	//3 points of our triangle as xyz
	float vertices[] = {
		0.5f, -0.5f, 0, //bottom right
		-0.5f, -0.5f, 0, //bottom left
		0.0f, 0.5f, 0 //top
	};
	//these current positions are based on the the NDC(normalised device coordinates where x and y span -1 to 1)

	//4 point rectangle
	float rectVertices[] =
	{
		0.5f, 0.5f, 0, //top right            index: 0
		0.5f, -0.5f, 0, //bottom right        index: 1
		-0.5, -0.5f, 0, // bottom left        index: 2
		-0.5f, 0.5f, 0 //top left             index: 3
	};

	//order of indexes(indices) to connect up to make 2 triangles
	unsigned int indices[] = {
		0, 1, 3, //first triangle
		1, 2, 3 //second triangle
	};

	float colourRectVertices[] = {
		// positions // colors // texture coords
		//x    y    z      R     G    B      Ts   Tt
		0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top right              0
		0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom right       1
		-0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // bottom left         2
		-0.5f, 0.5f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f // top left            3
	};


	//VBO - Vertex Buffer Object
	//currently vertices is stored in RAM and we want stored on the GPU somewhere. So we make a buffer object for it to store this data on the gpu
	unsigned int vboID1; //we need to store the id of the created vertex buffer object somewhere to use it in our code when we want to
	//generate a VBO and set the new VBO's ID into our vboID1 variable.
	glGenBuffers(1, &vboID1);//params: how many VBO's to make, where to store the new VBO's ids
	//example of generating multiple at a time
	//use array instead: unsigned int VBOs[6];
	//glGenBuffers(6, VBOs);//will feed the 6 new VBO ids into the 6 slots of the array

	//generate a Vertex Array Object(VAO), helps describe what the raw VBO data is
	unsigned int vaoID1;
	glGenVertexArrays(1, &vaoID1);

	//bind this vao to be the current working vao right now
	glBindVertexArray(vaoID1);

	//you can only have 1 VBO bound at a time in opengl, but you can swap whenever you want. Lets bind our first VBO
	glBindBuffer(GL_ARRAY_BUFFER, vboID1);//params: type of buffer to bind, the id of the buffer I want to bind to

	//pass data to the currently binded VBO on the graphics card
	//params: type of buffer,
	//		  size of data to give to the buffer
	//        data to give to the buffer
	//		  type of data: static, dynamic, stream
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	//using current VAO, record how to pass vertex data from our VBO to our Vertex Shader
	//params: 0 = location 0 slot of the vertex shader (for us, thats where aPos is located)
	//		 aPos is expecting 3 values (vec3), so we'll pass it 3 values at a time
	//       each of the 3 values are GL_FLOATs
	//		 Normalise it? (if  < -1 or >1, cut the numbers down), false means no, dont screw with my vertex numbers
	//		 stride, size of how many floats to jump at a time to find the next vertex (3 values make up x,y,z so to get to the second ,x,y,z we jump 3)
	//		 how many floats into this array is where I should start reading this information (0 means start) NOTE: must always be void* for this one
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	//turn on the location 0 slot(aPos) in the vertex shader
	glEnableVertexAttribArray(0);// this is where vec3 aPos is located, location = 0

	//bind to no VAO so we dont accidentally screw up our VAO info
	glBindVertexArray(0);


	//RECTANGLE STUFF
	unsigned int vboID2;
	glGenBuffers(1, &vboID2);

	unsigned int vaoID2;
	glGenVertexArrays(1, &vaoID2);

	//Elemental Buffer Object(EBO) which will hold the indices to help describe how to draw the triangles making up our rectangle
	unsigned int eboID1;
	glGenBuffers(1, &eboID1);

	//lets setup the vao, vbo and ebo
	glBindVertexArray(vaoID2);
	glBindBuffer(GL_ARRAY_BUFFER, vboID2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectVertices), rectVertices, GL_STATIC_DRAW);
	//bind the EBO so we can start feeding it data
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboID1);
	//giv it the data of the indices
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	//describe the the vertex data to the VAO
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	//enable aPos(location 0) for the vertex shader
	glEnableVertexAttribArray(0);
	
	//unbind VAO when done (not necessary, but a good idea for house keeping)
	glBindVertexArray(0);


	//TEXTURED RECTANGLE VAO,VBO and EBO
	//CREATE VBO TO STORE VERTICES
	unsigned int textureRectVBO;
	glGenBuffers(1, &textureRectVBO);
	//CREATE EBO TO STORE CONNECTION INDEXES 
	unsigned int textureRectEBO;
	glGenBuffers(1, &textureRectEBO);
	//CREATE VAO TO STORE OPERATIONS ON VBO
	unsigned int textureRectVAO;
	glGenVertexArrays(1, &textureRectVAO);
	//to work with this VAO bind it to make it the current one
	glBindVertexArray(textureRectVAO);
	//bind the buffer object to this vao
	glBindBuffer(GL_ARRAY_BUFFER, textureRectVBO);
	//give data to VBO
	glBufferData(GL_ARRAY_BUFFER, sizeof(colourRectVertices), colourRectVertices, GL_STATIC_DRAW);

	//bind elemental buffer object to this vao
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, textureRectEBO);
	//give indices to ebo
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	//tell VAO which part of the VBO is for location = 0 of our vertex shader
	//Position(x,y,z)
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	//onto location = 1
	//Colour(R,G,B)
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
	//last, location = 2
	//Texture Coordinate(S,T)
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);
	//unbind our VAO
	glBindVertexArray(0);

	//generate a texture in the gfx card and return its id
	unsigned int texture1ID;
	glGenTextures(1, &texture1ID);
	//to make this texture the one we are working on, bind it
	glBindTexture(GL_TEXTURE_2D, texture1ID);
	//wrapping options (how to continue the texturing past the points described on the model)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); //wrap on s(x axis), repeat
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);//wrap on t(y axis), mirrored
	//other wrapping options: GL_CLAMP_TO_BORDER, GL_CLAMP_TO_EDGE
	//filtering options
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //min on shrink: GL_LINEAR(bilinear filter, avg colour) or 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //mag on stretch: GL_NEAREST (try keep pixels as is, no blurring)
	//LOAD UP Crate Texture
	int width, height, numberChannels;//as we load up an image, these variables will be filled in
	unsigned char* image1Data = stbi_load("crate.jpg", &width, &height, &numberChannels, 0);
	//if it loaded
	if (image1Data){
		cout << "Crate loaded successfully!" << endl;
		//lets pass the image data to the texture in gfx card
		//params: texture type,
		// 0 = mipmap level(if you want to set it manually, otherwise, set to zero)
		//GL_RGB = format we want to store the texture data in on the gfx card
		//width/height = size of the texture
		//0 = must always be zero, some old legacy code shit
		//GL_RGB = the current format this image is in( jpg usually RGB)
		//GL_UNSIGNED_BYTE = how this data has been loaded up (unsigned char = 1 btye, so this equivalent)
		//image1Data = our data
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image1Data);//copy from ram to vram

		//generate mipmaps for this texture
		glGenerateMipmap(GL_TEXTURE_2D);

	}
	else
	{
		cout << "Crate failed!" << endl;
	}
	//cleanup image memory
	stbi_image_free(image1Data);

	//generate a texture in the gfx card and return its id
	unsigned int texture2ID;
	glGenTextures(1, &texture2ID);
	//to make this texture the one we are working on, bind it
	glBindTexture(GL_TEXTURE_2D, texture2ID);
	//wrapping options (how to continue the texturing past the points described on the model)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); //wrap on s(x axis), repeat
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);//wrap on t(y axis), mirrored
	//other wrapping options: GL_CLAMP_TO_BORDER, GL_CLAMP_TO_EDGE
	//filtering options
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //min on shrink: GL_LINEAR(bilinear filter, avg colour) or 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //mag on stretch: GL_NEAREST (try keep pixels as is, no blurring)
	//LOAD UP Crate Texture
	//int width, height, numberChannels;//as we load up an image, these variables will be filled in
	unsigned char* image2Data = stbi_load("airplane.png", &width, &height, &numberChannels, 0);
	//if it loaded
	if (image2Data){
		cout << "plane loaded successfully!" << endl;
		//lets pass the image data to the texture in gfx card
		//params: texture type,
		// 0 = mipmap level(if you want to set it manually, otherwise, set to zero)
		//GL_RGB = format we want to store the texture data in on the gfx card
		//width/height = size of the texture
		//0 = must always be zero, some old legacy code shit
		//GL_RGBA = the current format this image is in( jpg usually RGB)
		//GL_UNSIGNED_BYTE = how this data has been loaded up (unsigned char = 1 btye, so this equivalent)
		//image1Data = our data
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image2Data);//copy from ram to vram

		//generate mipmaps for this texture
		glGenerateMipmap(GL_TEXTURE_2D);

	}
	else
	{
		cout << "plane failed!" << endl;
	}
	//cleanup image memory
	stbi_image_free(image2Data);

	RainbowTriangle rainbowTriangle;

	//array of 10 cubes
	Cube cubes[10];

	glm::vec3 cubePositions[] = {
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(2.0f, 5.0f, -15.0f),
		glm::vec3(-1.5f, -2.2f, -2.5f),
		glm::vec3(-3.8f, -2.0f, -12.3f),
		glm::vec3(2.4f, -0.4f, -3.5f),
		glm::vec3(-1.7f, 3.0f, -7.5f),
		glm::vec3(1.3f, -2.0f, -2.5f),
		glm::vec3(1.5f, 2.0f, -2.5f),
		glm::vec3(1.5f, 0.2f, -1.5f),
		glm::vec3(-1.3f, 1.0f, -1.5f)
	};

	//for each cube, give it the corresponding position and set its shader
	for (int i = 0; i < 10; i++){
		cubes[i].pos = cubePositions[i];
		cubes[i].shader = &shaderProgram4;
	}

	//GAME LOOP
	while (!glfwWindowShouldClose(window)){
		//update time management stuff
		float currentFrame = (float)glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		cout << "dt = " << deltaTime << endl;


		//user inputs
		processInputs(window);
		//RENDER STUFF
		//set clear colour
		glClearColor(0, 0, 1, 1);//rgba, values 0-1
		//clear screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//DRAW THE MOTHERFUCKIN TRIANGLE!!
		/*
		//tell it which shader to use
		glUseProgram(shaderProgramID);
		//we then bind the VAO we want to draw from
		glBindVertexArray(vaoID1);
		//draw the vertex array
		//params: primitive type (triangles, points, line_strip)
		//		  starting index to start drawing from in our VBO data
		//		  how many vertexes to draw from our VBO
		glDrawArrays(GL_TRIANGLES, 0, 3);
		*/
		if (currentShape == 1){
			//DRAW THE MOTHERFUCKIN RECTANGLE!!!
			//glUseProgram(shaderProgramID);
			//shaderProgram1.use();
			shaderProgram2.use();

			//work out our greencolour
			float greenValue = sin(glfwGetTime()) / 2.0f + 0.5f;


			//get ourColour uniform location from the shader program, so we can set its value
			int ourColourUniformLocation = glGetUniformLocation(shaderProgram2.ID, "ourColour");
			//now we set the value
			//                                     R     G           B     A
			glUniform4f(ourColourUniformLocation, 0.0f, greenValue, 0.0f, 1.0f);
			//NOTE: uniforms are global, so ourColour will stay green forever until its changed to something else

			//bind vao2
			glBindVertexArray(vaoID2);
			//draw using elements(indices)
			//params: primitive type
			//		  how many elements in our indices array(or how many we would like to use)
			//		  data type of the indices
			//		  where to start in the indices array
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		}
		if (currentShape == 2){
			//DRAW RAINBOW TRIANGLE
			rainbowTriangle.draw();
		}
		if (currentShape == 3){
			//gonna draw ontop of everything else with our textured rectangle
			//DRAW TEXTURED CUBES
			for (int i = 0; i < 10; i++){

				//transform local model space to world space
				glm::mat4 model = glm::mat4(1.0f);
				//move our rect on x axis
				model = glm::translate(model, cubes[i].pos);
				//rotate on y axis
				model = glm::rotate(model, (float)(glfwGetTime()), glm::vec3(0.0f, 1.0f, 0.0f));

				//transforms world based on camera position
				glm::mat4 view = camera.GetViewMatrix();//glm::mat4(1.0f);
				//camera moves in the reverse of what we expect mathematically(move camera back, which ends up pushing all objects in scene forward instead)
				//view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));
				/*view = glm::lookAt(glm::vec3(0.0f, 4.0f, 3.0f), //camera position in the world
					cubes[3].pos, //target position
					 glm::vec3(0.0f, 1.0f, 0.0f));//normalised up vector (between -1 and 1), so up for us is positive on the y axis*/
				/*float radius = 10.0f;
				float camX = sin(glfwGetTime())*radius;
				float camZ = cos(glfwGetTime())*radius;
				view = glm::lookAt(glm::vec3(camX, 0.0f, camZ), //cam pos
					glm::vec3(0.0f, 0.0f, 0.0f), //target
					glm::vec3(0.0f, 1.0f, 0.0f) //up vector
					);*/


				//lets apply perspective and convert all coordinates to the our -1 to 1 Normalised Device Coordinates (NDC)
				glm::mat4 projection = glm::mat4(1.0f);
				//                         Field of view angle,  screen aspect ration, near distance, far distance
				projection = glm::perspective(glm::radians(camera.Zoom), 800.0f / 600.0f, 0.1f, 100.0f);

				//apply all these matrices onto our shader
				glUniformMatrix4fv(glGetUniformLocation(shaderProgram4.ID, "model"), 1, GL_FALSE, glm::value_ptr(model));
				glUniformMatrix4fv(glGetUniformLocation(shaderProgram4.ID, "view"), 1, GL_FALSE, glm::value_ptr(view));
				glUniformMatrix4fv(glGetUniformLocation(shaderProgram4.ID, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

				//make first texture slot active(0-15 slots)
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, texture1ID);

				//set uniform value for fragmentShader's texture1:
				//1. get uniforms location
				int texture1UniformLocation = glGetUniformLocation(shaderProgram4.ID, "texture1");
				//2. set texture1 uniform to use texture in slot 0
				glUniform1i(texture1UniformLocation, 0);

				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, texture2ID);
				//tell fragment shader that its texture2 variable should reference texture slot 1
				glUniform1i(glGetUniformLocation(shaderProgram4.ID, "texture2"), 1);

				cubes[i].draw();
			}
		}
		//process any polled events
		glfwPollEvents();

		//openGL uses double buffering, so to make these render changes visual
		//we swap the working buffer with the currently showing buffer
		glfwSwapBuffers(window);

		showFPS(window);
	}

	glfwTerminate();
}

//window resize call back function, which will change glViewport to match new size
void windowResizeCallBack(GLFWwindow* window, int width, int height){
	glViewport(0, 0, width, height);
}

//user input function
void processInputs(GLFWwindow* window){
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS){
		glfwSetWindowShouldClose(window, true);
	}
	else if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS){
		currentShape = 1;
	}
	else if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS){
		currentShape = 2;
	}
	else if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS){
		currentShape = 3;
	}
	else if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS){
		//flip wireframe mode
		wireFrame = !wireFrame;
		if (wireFrame)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);//default
	}

	//move camera based on w,a,s,d keys
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, deltaTime);
}

//show frames per second
void showFPS(GLFWwindow* window){
	static double previousSeconds = 0.0;
	static int frameCount = 0;
	double elapsedSeconds;
	double currentSeconds = glfwGetTime();//number of seconds since start of program

	elapsedSeconds = currentSeconds - previousSeconds;
	if (elapsedSeconds > 0.25){
		previousSeconds = currentSeconds;
		double fps = (double)frameCount / elapsedSeconds;
		double msPerFrame = 1000.0 / fps;

		stringstream ss;
		ss.precision(3);//3  decimal places
		ss << fixed << "FPS: " << fps << " Frame Time: " << msPerFrame << "(ms)" << endl;

		glfwSetWindowTitle(window, ss.str().c_str());

		frameCount = 0;
	}

	frameCount++;
}

//new callback functions for mouse input!
void mouse_callback(GLFWwindow* window, double xpos, double ypos){
	//when mouse first read, its xpos and ypos might not be anywhere near the centre of the screen, so this helps for that scenario so the camera
	//doesn't rotate too far
	if (firstMouse){
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}
	
	//work out how many pixels the mouse has moved on both x and y axis
	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; //reverse math because y axis of screen is positive going down
	//update last x and y
	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);

}

//scroll wheel
void scroll_callback(GLFWwindow* window, double xOffset, double yOffset){
	camera.ProcessMouseScroll(yOffset);//changes FOV for camera
}