#version 330 core

//value passed in
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColour;
layout (location = 2) in vec2 aTexCoord; //texture coordinate associated with this position

//will be passed to fragment shaders in of the same name
out vec4 vertexColour;
out vec2 TexCoord; //to pass onto fragment shader

uniform mat4 transform;

void main(){
	//vertex shader MUST set a vec4 value for gl_position to help with
	//shape assembly
	gl_Position = transform*vec4(aPos, 1.0);
	
	vertexColour = vec4(aColour, 1.0);
	TexCoord = aTexCoord;

}

