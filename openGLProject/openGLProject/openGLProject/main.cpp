#include <glad\glad.h>
#include <GLFW\glfw3.h>
#include <iostream>
#include <string>

using namespace std;

//window resize call back function prototype
void windowResizeCallBack(GLFWwindow* window, int width, int height);

//user inputs
void processInputs(GLFWwindow* window);

void main()
{
	glfwInit();
	//tell glfw that we want to work with openGL 3.3 core profile
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //the first 3 of 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //the .3 of 3.3
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //core profile

	//build window
	GLFWwindow *window = glfwCreateWindow(800, 600, "My first OpenGL thing", NULL, NULL);
	//if it fails
	if (window == NULL){
		//try report error
		cout << "failed to create window" << endl;
		glfwTerminate(); //cleanup glfw stuff
		system("pause");
		return;
	}
	//make this window the current one
	glfwMakeContextCurrent(window);

	//initialise GLAD
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)){
		//if this fails, then
		cout << "GLAD failed to initialise" << endl;
		glfwTerminate(); //cleanup glfw stuff
		system("pause");
		return;
	}

	//set up openGL viewport x,y,w,h
	glViewport(0, 0, 800, 600);//you dont have to use the full window for openGL but we will

	//add window resize callback, params: window to check events on, function to call
		//when it resizes
	glfwSetFramebufferSizeCallback(window, windowResizeCallBack);

	//GAME LOOP
	while (!glfwWindowShouldClose(window))
	{
		//user inputs
		processInputs(window);
		//GROWTH
		//MINDSET!

		//RENDER STUFF
		//set openGL clear colour
		glClearColor(0,0,1,1);//r,g,b,a as floats, 0 to 1
		//clear screen
		glClear(GL_COLOR_BUFFER_BIT);

		//Input for window
		glfwPollEvents();

		//swap render buffers with this loops rendered scene
		glfwSwapBuffers(window);
	}

	glfwTerminate();
}

//window resize call back function prototype
void windowResizeCallBack(GLFWwindow* window, int width, int height){
	glViewport(0, 0, width, height);
}

//user inputs
void processInputs(GLFWwindow* window){
	//if esc pressed, set window to 'should close'
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}
