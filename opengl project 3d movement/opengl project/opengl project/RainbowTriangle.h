#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <string>
#include <sstream>
#include <cmath>
#include "Shader.h"
class RainbowTriangle
{
public:
	unsigned int vbo;
	unsigned int vao;
	Shader* shader;


	RainbowTriangle();
	~RainbowTriangle();

	void draw();
};

