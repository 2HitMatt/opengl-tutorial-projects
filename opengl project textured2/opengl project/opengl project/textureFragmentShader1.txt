#version 330
out vec4 FragColor; //some vec4 must be out'd to decide final pixel colour

//catch values passed out from vertex shader with same name
in vec4 vertexColour;
in vec2 TexCoord;

//we need a sampler to hold reference to the texture we wish to use
//and this sample will help us spread/wrap the texture across our polygon or shape
uniform sampler2D texture1;

void main(){

	FragColor = texture(texture1, TexCoord);
}