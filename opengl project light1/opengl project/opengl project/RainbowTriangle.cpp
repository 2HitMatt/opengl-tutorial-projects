#include "RainbowTriangle.h"


RainbowTriangle::RainbowTriangle()
{
	shader = new Shader("vertexShader3.txt", "fragmentShader1.txt");
	
	float rainbowTriangleVertices[] = {
		//x  y   z        r  g  b
		0.5f, -0.5f, 0, 1, 0, 0,
		-0.5f, -0.5f, 0, 0, 1, 0,
		0.0f, 0.5f, 0, 0, 0, 1
	};

	//Build rainbow triangles vertex buffer and vertex array objects
	glGenBuffers(1, &vbo); //vbo - where to store vertices in the gfx card

	glGenVertexArrays(1, &vao); //vao - describes the data in the vbo to the vertex shader

	//bind means: make it the current working one
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	//give buffer the array of data
	glBufferData(GL_ARRAY_BUFFER, sizeof(rainbowTriangleVertices), rainbowTriangleVertices, GL_STATIC_DRAW);
	//params: 
	//vertex shader location, how many values it needs, datatype, 
	//stride(how many jumps on the array to get to the next XYZ), how many floats into the array do XYZ values start
	//the XYZ part of the rainbowTriangleVertices array
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0); //enable location = 0

	//next is the RGB part of the rainbowTriangleVertices array
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	glBindVertexArray(0); //unbind vao
	
}


RainbowTriangle::~RainbowTriangle()
{
	delete shader;
}

void RainbowTriangle::draw(){
	//DRAW RAINBOW TRIANGLE
	shader->use();
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 3);
}