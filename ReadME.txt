REFERENCE
==========
These are projects based on https://learnopengl.com/ tutorials. I'm sharing these projects so people can grab a working project if they ever get stuck or need reference. I commented the code as thoroughly as I could, which may be a bit much in the end.

PROJECT SETUP NOTE
==================
Projects may be expecting extra libraries glfw, glad, glm and assimp in C drive. 

To change the location they are looking in, right click the project(not solution) in the solution explorer and choose properties, in the VC++ directories you can change where it is looking for these extra files.

Cheers,
Matt